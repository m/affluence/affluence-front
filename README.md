# Affluence

Client Angular associé au backend [Affluence Serv](https://gitlab.metromobilite.fr/m/affluence/affluence-serv)

## Installation

`npm install`

## Exécution

* `npm run start` pour lancer l'application en local
* `ng run build` pour compiler l'application avec la configuration de développement.
* `ng run build:prod` pour compiler l'application avec la configuration de production.