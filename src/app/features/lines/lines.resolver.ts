import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LinesService } from './lines.service';

@Injectable({
	providedIn: 'root'
})
export class LinesResolver implements Resolve<any> {

	constructor(private linesService: LinesService) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (this.linesService.lines) {
			return this.linesService.lines;
		}
		return new Promise((resolve) => {
			this.linesService.getLines().subscribe((lines) => {
				this.linesService.lines = lines;
				resolve(lines);
			});
		});
	}

}
