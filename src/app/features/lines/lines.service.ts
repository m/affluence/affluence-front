import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { domain } from '@helpers/domain.helpers';
import { Line } from '@features/line/line.model';

@Injectable({
	providedIn: 'root'
})
export class LinesService {

	lines: Line[];

	constructor(private http: HttpClient) { }

	getLines(): Observable<Line[]> {
		return this.http.get<Line[]>(`${domain}/api/routers/default/index/routes`);
	}

	find(id: string): Line {
		return this.lines.find((line) => line.id === id);
	}
}
