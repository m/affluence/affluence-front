import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LineService } from './line.service';
import { LinesResolver } from '@features/lines/lines.resolver';

@Injectable({
	providedIn: 'root'
})
export class LineResolver implements Resolve<any> {

	constructor(
		private lineService: LineService,
		private linesResolver: LinesResolver
	) { }

	async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		await this.linesResolver.resolve(route, state);
		return new Promise((resolve) => {
			this.lineService.getClusters(route.params.id).subscribe((clusters) => {
				resolve(clusters);
			});
		});
	}

}
