import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { domain } from '@helpers/domain.helpers';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class LineService {

	constructor(private http: HttpClient) { }

	getClusters(lineId: string): Observable<any> {
		return this.http.get(`${domain}/api/routers/default/index/routes/${lineId}/clusters`);
	}
}
