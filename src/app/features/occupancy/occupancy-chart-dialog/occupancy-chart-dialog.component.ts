import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NextStop } from '@features/realtime-data/realtime-data.model';
import { Axis, Point, Dataset } from '@components/chart/chart.model';

@Component({
	templateUrl: './occupancy-chart-dialog.component.html',
	styleUrls: ['./occupancy-chart-dialog.component.scss']
})
export class OccupancyChartDialogComponent implements OnInit {

	axis: { x?: Axis, y?: Axis } = {
		x: {
			tooltipFormatter: (value, point: Point, dataset: Dataset) => {
				let end = '';
				dataset.points.forEach(group => {
					group.forEach((p, i, list) => {
						if (p.x === point.x) {
							end = list[i + 1] ? list[i + 1].x as string : '23:59';
						}
					});
				});
				return `${value.split(':').join('h')} et ${end.split(':').join('h')}`;
			},
		},
	};

	constructor(@Inject(MAT_DIALOG_DATA) public data: NextStop) { }

	ngOnInit(): void {
	}

}
