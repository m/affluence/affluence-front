export interface StopOccupancy {
	timeSlots: string[];
	currentTimeSlot: number;
	occupancy: Occupancy;
}

export interface Occupancy {
	routeDirection: { [key: string]: number[] };
}
