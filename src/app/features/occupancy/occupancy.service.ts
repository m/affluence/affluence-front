import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { domain } from '@helpers/domain.helpers';
import { StopOccupancy } from './occupancy.model';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class OccupancyService {

	constructor(private http: HttpClient) { }

	getOccupancy(stopId: string): Observable<StopOccupancy> {
		return this.http.get<StopOccupancy>(`${domain}/api/stops/${stopId}/occupancy/json`);
	}
}
