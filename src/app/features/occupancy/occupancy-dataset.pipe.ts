import { Pipe, PipeTransform } from '@angular/core';
import { OccupancyService } from './occupancy.service';
import { Dataset, Axis } from '@components/chart/chart.model';
import { NextStop } from '@features/realtime-data/realtime-data.model';
import { Observable } from 'rxjs';
import { StopOccupancy } from './occupancy.model';

const fakeResponse: StopOccupancy = {
	timeSlots: ['00:00', '06:45', '08:45', '11:30', '13:30', '15:30', '18:30', '21:00'],
	currentTimeSlot: 5,
	occupancy: {
		routeDirection: {
			'SEM:C:1': [156, 2825, 1800, 1121, 1349, 2255, 944, 420]
		}
	}
};

@Pipe({
	name: 'occupancyDataset'
})
export class OccupancyDatasetPipe implements PipeTransform {

	constructor(private occupancyService: OccupancyService) { }

	transform(obj: NextStop, axis?: { x?: Axis, y?: Axis, }): Observable<Dataset> {
		const routeDirection = obj.pattern.dir;
		return new Observable<Dataset>(observer => {
			observer.next(this.toDataset(fakeResponse, obj.ligne.id, 1, axis, true));
			this.occupancyService.getOccupancy(obj.stopId).subscribe(response => {
				const dataset = this.toDataset(response, obj.ligne.id, routeDirection, axis, false);
				observer.next(dataset);
				observer.complete();
			});
		});
	}

	private toDataset(data: StopOccupancy, ligne : string, routeDirection: number, axis?: { x?: Axis, y?: Axis, },  ghost = false): Dataset {
		let test = [];
		let unavailable : boolean = true;
		if(typeof data !== 'undefined' && typeof ligne !== 'undefined' && typeof data[ligne] !== 'undefined' && typeof data[ligne][routeDirection] !== 'undefined' && typeof data[ligne][routeDirection]['times'] !== 'undefined'){
			for (const horaire in data[ligne][routeDirection]['times']) {
				let myHeure : number = +horaire;
				if(myHeure >= 21600 && data[ligne][routeDirection]['times'][horaire]['percent'] > 2) {
					test.push({x: horaire, y: data[ligne][routeDirection]['times'][horaire]['percent']});
					unavailable = false;
				}
			}
		}

		return {
			points: [test],
			axis,
			options: {
				padding: [4, 0, 8, 10],
				unavailable: unavailable,
				ghost
			}
		};

/* 		if (data?.occupancy?.routeDirection && routeDirection in data.occupancy.routeDirection) {
			const points = data.timeSlots.map((time, index, list) => {
				return {
					x: time,
					y: data.occupancy.routeDirection[routeDirection][index]
				};
			});
			const isEmpty = points.every(p => p.y === 0);
			dataset = {
				points: [[]],
				axis,
				options: {
					padding: [4, 0, 8, 10],
					empty: isEmpty,
					ghost
				}
			};
			if (!isEmpty) {
				// Add a fake value to display the last column correctly.
				points.push({ x: '23:59', y: 0 });
				dataset.points = [points];
			}
		} else {
			console.log('else', test.length);
			dataset = {
				points: [[]],
				axis,
				options: {
					padding: [4, 0, 8, 10],
					unavailable: false,
					ghost
				}
			}
		}
		return dataset; */
	}

}
