import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { domain } from '@helpers/domain.helpers';
import { Observable } from 'rxjs';
import { Pattern } from './patterns.model';

@Injectable({
	providedIn: 'root'
})
export class PatternsService {

	constructor(private http: HttpClient) { }

	getPatterns(clusterId: string): Observable<{ [key: string]: Pattern[] }> {
		return this.http.get<{ [key: string]: Pattern[] }>(`${domain}/api/clusters/${clusterId}/patterns`);
	}
}
