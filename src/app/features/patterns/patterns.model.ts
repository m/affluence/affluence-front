import { Line } from '@features/line/line.model';

export interface Pattern {
	// BE properties:
	id: string;
	desc: string;
	dir: number;
	shortDesc: string;
	lastStop: string;
	lastStopName: string;
	// FE properties:
	line?: Line;
	stopId: string;
	_id: string; // use to remove duplicates.
}
