import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LinesResolver } from '@features/lines/lines.resolver';
import { PatternsService } from './patterns.service';
import { LinesService } from '@features/lines/lines.service';
import { groupByField } from '@helpers/array.helper';

@Injectable({
	providedIn: 'root'
})
export class PatternsResolver implements Resolve<any> {

	constructor(
		private patternsService: PatternsService,
		private linesResolver: LinesResolver,
		private linesService: LinesService
	) { }

	async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		await this.linesResolver.resolve(route, state);
		return new Promise((resolve) => {
			this.patternsService.getPatterns(route.params.cluster).subscribe((response) => {
				const patterns = [];
				Object.entries(response).forEach(([stopId, _patterns]) => {
					_patterns
						.map(p => {
							const [prefix, line, _, __] = p.id.split(':');
							p.line = this.linesService.find(`${prefix}:${line}`);
							p._id = `${prefix}:${line}:${p.dir}`;
							p.stopId = stopId;
							return p;
						})
						.forEach((pattern) => {
							// Remove duplicates.
							if (!patterns.find(p => pattern._id === p._id)) {
								patterns.push(pattern);
							}
						});
				});
				resolve(groupByField(patterns, 'line.id'));
			});
		});
	}

}
