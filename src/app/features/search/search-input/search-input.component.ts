import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { debounceTime, filter, tap, switchMap, finalize, map } from 'rxjs/operators';
import { SearchService } from '../search.service';
import { SearchResponse } from '../search.model';
import { trigger, transition, style, animate } from '@angular/animations';
import { of } from 'rxjs';

@Component({
	selector: 'app-search-input',
	templateUrl: './search-input.component.html',
	styleUrls: ['./search-input.component.scss'],
	exportAs: 'searchInput',
	animations: [
		trigger('fade', [
			transition(':enter', [
				style({ opacity: 0 }),
				animate('250ms', style({ opacity: 1 }))
			]),
			transition(':leave', [
				style({ opacity: 1 }),
				animate('250ms', style({ opacity: 0 }))
			]),
		])
	]
})
export class SearchInputComponent implements OnInit {

	@Input() types: string;

	@Input() set required(value: boolean) {
		this._required = value !== undefined;
	}
	get required(): boolean {
		return this._required;
	}


	@Output() search = new EventEmitter<any>();

	searchControl = new UntypedFormControl();
	displayLoader: boolean;

	private _required: boolean;

	constructor(private searchService: SearchService) { }

	ngOnInit(): void {
		this.searchControl.valueChanges
			.pipe(
				debounceTime(300),
				// filter(query => query.length >= 3),
				tap(() => this.displayLoader = true),
				switchMap((value: string) => {
					if (value.length >= 3) {
						return this.searchService.search(value, this.types);
					} else {
						return of({ features: [] });
					}
				}),
				map((data: SearchResponse) => {
					if (data && Array.isArray(data.features)) {
						data.features.sort((a: any, b: any) => {
							if (parseInt(a.properties.dist, 10) < parseInt(b.properties.dist, 10))
								return 1; // tri decroissant on inverse
							if (parseInt(a.properties.dist, 10) > parseInt(b.properties.dist, 10))
								return -1;
							// a doit être égal à b
							return 0;
						});
						data.features = data.features.slice(0, 30);
						return data;
					}
					return { type: '', features: [] };
				}),
			).subscribe((data: SearchResponse) => {
				this.search.emit(data.features);
				this.displayLoader = false;
			});
	}

	clear() {
		this.searchControl.patchValue('');
	}

}
