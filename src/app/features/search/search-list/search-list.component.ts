import { Component, OnInit, ContentChild, TemplateRef } from '@angular/core';
import { SearchService } from '../search.service';
import { SearchFeature } from '../search.model';
import { SearchItemDirective } from '../search-item.directive';

@Component({
	selector: 'app-search-list',
	templateUrl: './search-list.component.html',
	styleUrls: ['./search-list.component.scss'],
	exportAs: 'searchList'
})
export class SearchListComponent implements OnInit {

	searchItems = [];
	latestSearches: any[];

	@ContentChild(SearchItemDirective, { read: TemplateRef }) searchItemTemplate: TemplateRef<any>;

	constructor(private searchService: SearchService) { }

	ngOnInit(): void {
		this.latestSearches = this.searchService.latestSearches;
	}

	udpate(data: SearchFeature[]) {
		this.searchItems = data;
	}

	onSelectSearchItem(item: SearchFeature, addToLatestSearches = false) {
		if (addToLatestSearches) {
			this.searchService.addToLatestSearches(item);
		}
	}

}
