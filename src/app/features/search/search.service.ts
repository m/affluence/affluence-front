import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { domain } from '@helpers/domain.helpers';
import { Observable } from 'rxjs';
import { SearchResponse, SearchFeature } from './search.model';

const LOCAL_LATEST_KEY = 'recherches_recentes';

@Injectable({
	providedIn: 'root'
})
export class SearchService {

	private limit = 3;
	private _latestSearches: SearchFeature[] = [];

	constructor(private http: HttpClient) {
		this.load();
	}

	search(query: string, types: string): Observable<SearchResponse> {
		let params = new HttpParams({ fromObject: { query } });
		if (types !== '') {
			params = params.set('types', types);
		}
		return this.http.get<SearchResponse>(`${domain}/api/find/json`, { params });
	}

	find(type: string, code: string) {
		return this.http.get<any>(`${domain}/api/findType/json`, {
			params: new HttpParams({
				fromObject: {
					types: type,
					codes: code
				}
			})
		});
	}

	get latestSearches(): SearchFeature[] {
		return this._latestSearches;
	}

	save(searches: SearchFeature[]) {
		localStorage.setItem(LOCAL_LATEST_KEY, JSON.stringify(searches));
	}

	addToLatestSearches(obj: SearchFeature) {
		const alreadySearched = !!this._latestSearches.find(ls => ls.properties.id === obj.properties.id);
		if (alreadySearched) {
			this._latestSearches = this._latestSearches.filter(ls => ls.properties.id !== obj.properties.id);
		}
		this._latestSearches.unshift(obj);
		this._latestSearches.splice(this.limit);
		this.save(this._latestSearches);
	}

	private load() {
		this._latestSearches = JSON.parse(localStorage.getItem(LOCAL_LATEST_KEY)) || [];
	}
}
