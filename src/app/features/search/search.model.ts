export interface SearchResponse {
	type: string;
	features: SearchFeature[];
}

export interface SearchFeature {
	type: string;
	properties: SearchProperties;
	geometry: SearchGeometry;
}

export interface SearchGeometry {
	type: string;
	coordinates: number[];
}

export interface SearchProperties {
	LIBELLE: string;
	rue: string;
	COMMUNE: string;
	id: string;
	importance: number;
	numero: { [key: number]: { lat: number, lon: number } };
	type: string;
	LaMetro: boolean;
	LeGresivaudan: boolean;
	PaysVoironnais: boolean;
	dist: string;
	numeroRue: string;
}
