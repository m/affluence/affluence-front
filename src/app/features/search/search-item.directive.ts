import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
	selector: '[appSearchItem], [searchItem], [search-item]'
})
export class SearchItemDirective {

	constructor(public elementRef: ElementRef) { }

}
