import { Line } from '@features/line/line.model'
import { Pattern } from '@features/patterns/patterns.model';

export interface StopTime {
	pattern: Pattern;
	times: Time[];
}

export interface Time {
	stopId: string;
	stopName: string;
	scheduledArrival: number;
	scheduledDeparture: number;
	realtimeArrival: number;
	realtimeDeparture: number;
	arrivalDelay: number;
	departureDelay: number;
	timepoint: boolean;
	realtime: boolean;
	serviceDay: number;
	tripId: number;
}


export interface NextStop {
	pattern: Pattern;
	name: string;
	ligne: Line;
	time: Time[];
	id: string;
	city: string;
	stopId?: string;
}
