import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { domain } from '@helpers/domain.helpers';
import { StopTime, Time, NextStop } from './realtime-data.model';
import { cleanString } from '@helpers/string.helper';
import { LinesService } from '@features/lines/lines.service';

@Injectable({
	providedIn: 'root'
})
export class RealtimeDataService {

	constructor(private http: HttpClient, private linesService: LinesService) { }

	getStoptimes(type: string, id: string, linesId: string[] = []): Observable<StopTime[]> {
		const params: any = {};
		if (linesId.length > 0) {
			params.route = linesId.join(',');
		}
		const headers = new HttpHeaders().set('Origin', domain);
		const options = { params : new HttpParams({ fromObject: params }), headers : headers};
		const url = `${this.getBaseUrl(type)}/${id}/stoptimes`;
		return this.http.get<StopTime[]>(url, options);
	}

	getBaseUrl(type: string) {
		let url = `${domain}/api/routers/default/index/clusters`;
		if (type === 'stops') {
			url = `${domain}/api/routers/default/index/stops`;
		}
		return url;
	}

	parseResultPP(response: StopTime[], nbEltToDisplay: number) {
		if (!response) return [];
		if (!this.linesService.lines || this.linesService.lines.length < 1) {
			console.error('The realtime-data service need linesService.lines to be populated => require the lineResolver or the LinesResolver on this route.');
			return;
		}
		const nextStops: { [key: string]: NextStop } = {};
		response.forEach(stopTime => {
			const lineId = stopTime.pattern.id.split(':')[0] + ':' + stopTime.pattern.id.split(':')[1];
			const line = this.linesService.find(lineId);
			if (!line) {
				return;
			}
			const desc = stopTime.pattern.desc;
			if (!nextStops[desc]) {
				const city = stopTime.pattern.lastStopName && stopTime.pattern.lastStopName.includes(',') ? stopTime.pattern.lastStopName.split(',')[0] : undefined;
				nextStops[desc] = {
					pattern: stopTime.pattern,
					name: desc,
					ligne: line,
					time: [],
					id: cleanString(`${line.id}:${stopTime.pattern.desc}`),
					city: city
				};
			}
			if (nbEltToDisplay) {
				for (let i = 0; i < stopTime.times.length && i < nbEltToDisplay; i++) {
					nextStops[desc].time.push(stopTime.times[i]);
				}
			} else {
				// tslint:disable
				for (let i = 0; i < stopTime.times.length; i++) {
					nextStops[desc].time.push(stopTime.times[i]);
				}
				// tslint:enable
			}
		});
		const prochPassag: NextStop[] = [];
		for (const el in nextStops) {
			if (nextStops[el].time.length > 0) {
				prochPassag.push(nextStops[el]);
			}
		}
		prochPassag.forEach(element => {
			element.time.sort((a, b) => {
				const depA = (a.realtimeDeparture ? a.realtimeDeparture : a.scheduledDeparture) + a.serviceDay;
				const depB = (b.realtimeDeparture ? b.realtimeDeparture : b.scheduledDeparture) + b.serviceDay;

				if (depA > depB) {
					return 1;
				}
				if (depA < depB) {
					return -1;
				}
				return 0;
			});
		});
		prochPassag.sort((a, b) => {
			if (!a || a.time.length === 0) return -1;
			if (!b.time || b.time.length === 0) return 1;
			const depA = (a.time[0].realtimeDeparture ? a.time[0].realtimeDeparture : a.time[0].scheduledDeparture) + a.time[0].serviceDay;
			const depB = (b.time[0].realtimeDeparture ? b.time[0].realtimeDeparture : b.time[0].scheduledDeparture) + b.time[0].serviceDay;
			if (depA > depB) {
				return 1;
			}
			if (depA < depB) {
				return -1;
			}
			return 0;
		});
		return prochPassag.filter((pp: any) => !pp.pattern.desc.includes('SANS VOYAGEUR'));
	}
}
