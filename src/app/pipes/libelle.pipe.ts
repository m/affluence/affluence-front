import { Pipe, PipeTransform } from '@angular/core';
import { DataService } from '@services/data.service';

@Pipe({
	name: 'libelle'
})
export class LibellePipe implements PipeTransform {

	constructor(private dataService: DataService) { }

	transform(value: any, ...args: any[]): string {
		return this.dataService.getLibellePoint(value);
	}

}
