import { Pipe, PipeTransform } from '@angular/core';
import { DataTypes } from '@helpers/types.helper';

@Pipe({
	name: 'id'
})
export class IdPipe implements PipeTransform {

	transform(value: any): unknown {
		return value[DataTypes.idField(value.type)];
	}

}
