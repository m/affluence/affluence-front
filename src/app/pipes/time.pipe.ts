import { Pipe, PipeTransform } from '@angular/core';

export function getTime(h: any): { value: string, unit: string } {
	let result = {
		value: '',
		unit: ''
	};
	if (h === '|' || h == null) return result = { value: '|', unit: '' };
	const dep = h.realtimeArrival;
	if (!dep) return;
	const currentTime = dep * 1000 + h.serviceDay * 1000;
	const now = new Date().getTime();
	let res = (currentTime - now) / 1000;

	if (((res / 60) < 1)) {
		result.value = '<1';
		result.unit = 'min';
	} else if ((res / 60) >= 1 && (res / 60) < 60) {
		res = Math.round(res / 60);
		result.value = res.toString();
		result.unit = 'min';
	} else {
		result.value = '>1';
		result.unit = 'h';
	}
	return result;
}

export function getTimeSchedule(h: any): string {
	if (h === '|' || !h) return h;
	const date = new Date();
	date.setHours(0, 0, 0, 0);
	const horodate = date.getTime() + (h * 1000);
	const dateToDisplay = new Date(horodate);
	// Fix samsung browser locale issue (12h instead of 24h).
	return [dateToDisplay.getHours(), `${dateToDisplay.getMinutes()}`.padStart(2, '0')].join(':');
}

@Pipe({
	name: 'time'
})
export class TimePipe implements PipeTransform {

	transform(h: any): string {
		return getTime(h).value;
	}

}

@Pipe({
	name: 'timeUnit'
})
export class TimeUnitPipe implements PipeTransform {

	transform(h: any): string {
		return getTime(h).unit;
	}

}

@Pipe({
	name: 'timeSchedule'
})
export class TimeSchedulePipe implements PipeTransform {

	transform(h: any): string {
		return getTimeSchedule(h);
	}

}
