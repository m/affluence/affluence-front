import { Pipe, PipeTransform } from '@angular/core';
import { cleanString } from '@helpers/string.helper';
import { getPropertyFromPath } from '@helpers/object.helper';

@Pipe({
	name: 'filter'
})
export class FilterPipe implements PipeTransform {

	transform(list: any[], path: string, query?: string | string[]): any[] {
		if (!query) {
			return list;
		}
		return list.filter((item: any) => {
			const field = cleanString(getPropertyFromPath(item, path));
			if (Array.isArray(query)) {
				return (query as string[]).some((q: string) => field.includes(cleanString(q)));
			}
			return field.includes(cleanString(query));
		});
	}

}
