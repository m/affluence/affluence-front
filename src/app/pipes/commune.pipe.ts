import { Pipe, PipeTransform } from '@angular/core';
import { DataService } from '@services/data.service';

@Pipe({
	name: 'commune'
})
export class CommunePipe implements PipeTransform {

	constructor(private dataService: DataService) { }

	transform(value: any, ...args: any[]): string {
		return this.dataService.getCommunePoint(value);
	}

}
