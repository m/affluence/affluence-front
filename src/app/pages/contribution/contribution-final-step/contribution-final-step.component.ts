import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { trigger, transition, style, animate, keyframes } from '@angular/animations';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { domain } from '@helpers/domain.helpers';

@Component({
	templateUrl: './contribution-final-step.component.html',
	styleUrls: ['./contribution-final-step.component.scss'],
	animations: [
		trigger('submit', [
			transition(':enter', [
				animate(600, keyframes([
					style({ transform: 'scale3d(1, 1, 1)', opacity: 0, offset: 0 }),
					style({ transform: 'scale3d(1.25, 0.75, 1)', opacity: 1, offset: 0.3 }),
					style({ transform: 'scale3d(0.75, 1.25, 1)', offset: 0.4 }),
					style({ transform: 'scale3d(1.15, 0.85, 1)', offset: 0.5 }),
					style({ transform: 'scale3d(.95, 1.05, 1)', offset: 0.65 }),
					style({ transform: 'scale3d(1.05, .95, 1)', offset: 0.75 }),
					style({ transform: 'scale3d(1, 1, 1)', offset: 1 }),
				]))
			])
		])
	]
})
export class ContributionFinalStepComponent implements OnInit {

	contributeForm: UntypedFormGroup;
	displaySubmitLoader: boolean;

	@ViewChild('snackBar', { static: true }) snackBarContent: TemplateRef<any>;

	constructor(
		private fb: UntypedFormBuilder,
		private activatedRoute: ActivatedRoute,
		private matIconRegistry: MatIconRegistry,
		private domSanitizer: DomSanitizer,
		private snackBar: MatSnackBar,
		private router: Router,
		private http: HttpClient
	) {
		this.matIconRegistry.addSvgIcon(
			'group',
			this.domSanitizer.bypassSecurityTrustResourceUrl('assets/group.svg')
		);
		this.matIconRegistry.addSvgIcon(
			'group_medium',
			this.domSanitizer.bypassSecurityTrustResourceUrl('assets/group_medium.svg')
		);
		this.matIconRegistry.addSvgIcon(
			'group_small',
			this.domSanitizer.bypassSecurityTrustResourceUrl('assets/group_small.svg')
		);
	}

	ngOnInit(): void {
		this.contributeForm = this.fb.group({
			// Hidden fields.
			stopId: [this.activatedRoute.snapshot.params.stop, Validators.required],
			routeId: [this.activatedRoute.snapshot.params.line, Validators.required],
			directionId: [this.activatedRoute.snapshot.params.dir, Validators.required],
			headsign: [this.activatedRoute.snapshot.params.headsign, Validators.required],
			// User inputs.
			time: [moment().format('HH:mm'), Validators.required],
			occupancy: [null, Validators.required]
		})
	}

	submit() {
		const [hours, minutes] = this.contributeForm.value.time.split(':');
		const values = {
			...this.contributeForm.value,
			time: +moment()
				.hour(parseInt(hours, 10))
				.minute(parseInt(minutes, 10))
		};
		const headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
		this.displaySubmitLoader = true;
		this.http.post(`${domain}/occupancy/report`, values, { headers }).subscribe(response => {
			this.displaySubmitLoader = false;
			this.snackBar.openFromTemplate(this.snackBarContent, {
				panelClass: 'contribute-submit-message',
				duration: 10000
			});
			this.router.navigate(['/']);
		}, error => {
			console.error(error);
			this.displaySubmitLoader = false;
			this.snackBar.open(`Une erreur s'est produite, veuillez réessayer plus tard.`, '', {
				duration: 10000,
			});
		});
	}

}
