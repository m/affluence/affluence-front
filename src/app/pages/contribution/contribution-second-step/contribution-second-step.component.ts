import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pattern } from '@features/patterns/patterns.model';

@Component({
	templateUrl: './contribution-second-step.component.html',
	styleUrls: ['./contribution-second-step.component.scss']
})
export class ContributionSecondStepComponent implements OnInit {

	patterns: Pattern[];

	constructor(
		private activatedRoute: ActivatedRoute
	) { }

	ngOnInit(): void {
		this.patterns = this.activatedRoute.snapshot.data.patterns;
	}

}
