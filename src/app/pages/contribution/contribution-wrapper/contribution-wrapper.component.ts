import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
	templateUrl: './contribution-wrapper.component.html',
	styleUrls: ['./contribution-wrapper.component.scss']
})
export class ContributionWrapperComponent implements OnInit, OnDestroy {

	total: number;
	step: number;

	private unsubscriber = new Subject<void>();

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private title: Title
	) { }

	ngOnInit(): void {
		this.total = this.activatedRoute.snapshot.data.totalStep;
		const title = this.activatedRoute.snapshot.data.title;
		this.step = this.activatedRoute.firstChild.snapshot.data.step;
		this.title.setTitle(title.replace('@step', this.step));
		this.router.events.pipe(
			filter(event => event instanceof NavigationEnd),
			takeUntil(this.unsubscriber))
			.subscribe(event => {
				this.step = this.activatedRoute.firstChild.snapshot.data.step;
				this.title.setTitle(title.replace('@step', this.step));
			});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}
}
