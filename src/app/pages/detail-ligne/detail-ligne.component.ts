import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, transition, animate, style } from '@angular/animations';
import { LinesService } from '@features/lines/lines.service';
import { Subject } from 'rxjs';
import { BreakpointService } from '@services/breakpoint.service';
import { takeUntil } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { Line } from '@features/line/line.model';

@Component({
	selector: 'app-detail-ligne',
	templateUrl: './detail-ligne.component.html',
	styleUrls: ['./detail-ligne.component.scss'],
	animations: [
		trigger('fade', [
			transition(':enter', [
				style({ opacity: 0, transform: 'scale(0)' }),
				animate('250ms', style({ opacity: 1, transform: 'scale(1)' }))
			]),
			transition(':leave', [
				style({ opacity: 1, transform: 'scale(1)' }),
				animate('250ms', style({ opacity: 0, transform: 'scale(0)' }))
			]),
		])
	]
})
export class DetailLigneComponent implements OnInit, OnDestroy {

	ligneArrets;
	ligne: Line;
	filtreArrets = '';
	splitLongName: string[];

	sidenavMode = 'side';
	sidenavOpened = false;
	isMobile: boolean;
	private unsubscriber = new Subject<void>();

	constructor(
		private linesService: LinesService,
		private route: ActivatedRoute,
		private breakpointService: BreakpointService,
		private title: Title,
	) { }

	ngOnInit() {
		this.breakpointService.breakpoint.pipe(takeUntil(this.unsubscriber)).subscribe((result) => {
			this.isMobile = !result.matches;
			this.sidenavMode = result.matches ? 'side' : 'over';
			this.sidenavOpened = result.matches ? true : false;
		});
		this.ligneArrets = this.route.snapshot.data.clusters.map((arret: any) => {
			arret.type = 'clusters';
			return arret;
		});
		this.ligne = this.linesService.find(this.route.snapshot.params.id);
		this.title.setTitle(this.route.snapshot.data.title.replace('@line', this.ligne.shortName));
		this.splitLongName = [];
		if (this.ligne.longName.includes(' / ')) {
			this.splitLongName = this.ligne.longName.split(' / ');
		} else if (this.ligne.longName.includes(' - ')) {
			this.splitLongName = this.ligne.longName.split(' - ');
		} else {
			this.splitLongName = [this.ligne.longName];
		}
		this.splitLongName = this.splitLongName.map((name) => name.trim()).reverse();
	}

	ngOnDestroy() {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	back() {
		history.back();
	}

}
