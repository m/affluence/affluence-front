import { Component, OnInit, OnDestroy } from '@angular/core';
import { LinesService } from '@features/lines/lines.service';
import { Line } from '@features/line/line.model';
import { BreakpointService } from '@services/breakpoint.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
	templateUrl: './horaires-lignes.component.html',
	styleUrls: ['./horaires-lignes.component.scss']
})
export class HorairesLignesComponent implements OnInit, OnDestroy {

	enableContrib = environment.enableContrib;
	lines: Line[] = [];
	sidenavMode = 'side';
	sidenavOpened = false;
	isMobile: boolean;
	private unsubscriber = new Subject<void>();

	constructor(
		private linesService: LinesService,
		private breakpointService: BreakpointService,
		private title: Title,
		private activatedRoute: ActivatedRoute
	) { }

	ngOnInit() {
		this.title.setTitle(this.activatedRoute.snapshot.data.title);
		this.lines = this.linesService.lines.filter(line => ['TRAM', 'CHRONO', 'PROXIMO'].includes(line.type));
		this.breakpointService.breakpoint.pipe(takeUntil(this.unsubscriber)).subscribe((result) => {
			this.isMobile = !result.matches;
			this.sidenavMode = result.matches ? 'side' : 'over';
			this.sidenavOpened = result.matches ? true : false;
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	onSearchClosed(result: any) {
		if (result) {
			// this.router.navigate(
			// 	['/detailpoi', result.feature.properties.type, result.feature.properties.code],
			// 	{ state: { feature: result.feature } }
			// );
		}
	}

}
