import { getPropertyFromPath } from '@helpers/object.helper';

export function sortByField(arr: any[], args: string[]): any[] {
	if (!arr) return [];
	/* let sortField:string = fields[fields.length-1]; */
	const order: string = args[1];
	arr.sort((a: any, b: any) => {
		if (!a || !b) return 1;
		const keyA = getPropertyFromPath(a, args[0]);
		const keyB = getPropertyFromPath(b, args[0]);
		if (order === 'asc') {
			return (keyA > keyB) ? 1 : -1;  // A bigger than B, sorting ascending
		} else return (keyA > keyB) ? -1 : 1;  // B bigger than A, sorting descending
	});
	return arr;
}

export function groupByField(arr: any[], path: string): any[] {
	if (!arr) return [];
	const obj = {};
	arr.forEach((elt: any) => {
		const key = getPropertyFromPath(elt, path);
		if (!obj[key]) obj[key] = [];
		obj[key].push(elt);
	});
	let newArray = [];
	for (const key in obj) {
		newArray = newArray.concat(obj[key]);
	}
	return newArray;
}
