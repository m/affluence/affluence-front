const cssDeclaration = window.getComputedStyle(document.documentElement);

export namespace Style {
	export function getProperty(name: string): string {
		return cssDeclaration.getPropertyValue(name);
	}
	export const spacing = getProperty('--spacing');
	export const shapeRadius = getProperty('--shape-radius');
	export const mpwaBreakpoint = getProperty('--mpwa-breakpoint');
}
