import { environment } from 'src/environments/environment';

let _domain = 'http://localhost:3000';
if (environment.api === 'test') {
	_domain = 'https://datatest.mobilites-m.fr';
}
if (environment.api === 'preprod') {
	_domain = 'https://data-pp.mobilites-m.fr';
}
if (environment.api === 'testOuProd') {
	if (['data.mobilites-m.fr'].includes(window.location.hostname)) {
		_domain = 'https://data.mobilites-m.fr';

	} else if (['data-pp.mobilites-m.fr'].includes(window.location.hostname)) {
		_domain = 'https://data-pp.mobilites-m.fr';
	} 

	else {
		_domain = 'https://data.mobilites-m.fr';
	}
}

export const domain = _domain;
