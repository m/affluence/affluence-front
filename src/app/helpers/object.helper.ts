
export function getPropertyFromPath(obj: any, path: string): any {
	const properties = path.split('.');
	let elt = obj;
	properties.forEach((property: string) => {
		if (elt.hasOwnProperty(property)) {
			elt = elt[property];
		}
	});
	if (elt === obj) {
		return null;
	}
	return elt;
}
