export namespace DataTypes {
	export const types = {
		clusters: {
			libelle: 'name', commune: 'city',
			iconMaxRes: 6, iconMinRes: 0, icon: { src: 'assets/map/p_clusters.svg', anchor: [0.5, 1] },
			linesNear: false,
			isEvent: false,
			idField: 'code',
		},
		stops: {
			libelle: 'name', commune: 'city',
			iconMaxRes: 1, icon: { src: 'assets/map/p_stops.png', anchor: [0.5, 1] },
			linesNear: false,
			isEvent: false,
			idField: 'id',
		},
		dat: {
			libelle: 'ARRET',
			iconMaxRes: 2, icon: { src: 'assets/map/p_Dat.png', anchor: [0.5, 1] },
			linesNear: false,
			isEvent: false,
			idField: 'id',
		},
		agenceM: {
			libelle: 'NOM', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_agenceM.svg', anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		rue: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_free.png', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		lieux: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_free.png', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		PAR: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 5, icon: { src: 'assets/map/p_PR_0.svg', anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		PKG: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 5, icon: { src: 'assets/map/p_PKG_0.svg', anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		CAM: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/CAM_1.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: false,
			idField: 'id',
		},
		position: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/CAM_1.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: false,
			idField: 'id',
		},
		recharge: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 5, icon: { src: 'assets/map/p_recharge.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		autostop: {
			libelle: 'Nom du point', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_autostop.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		citiz: {
			libelle: 'name', commune: 'COMMUNE',
			iconMaxRes: 5, icon: { src: 'assets/map/p_citiz.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		citizyea: {
			libelle: 'name', commune: 'COMMUNE',
			iconMaxRes: 5, icon: { src: 'assets/map/p_citizyea.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		parkingCov: {
			libelle: 'nom_lieu', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_parkingCov.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		pointCov: {
			libelle: 'nom_lieu', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_pointCov.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		pointService: {
			libelle: 'NOM', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_pointService.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		MVA: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_MV_agence.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'CODE',
		},
		MVC: {
			libelle: 'LIBELLE', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_MV_Consigne.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'CODE',
		},
		depositaire: {
			libelle: 'NOM', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_relaisTAG.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'id',
		},
		hydrogene: {
			libelle: 'nom', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_hydrogene.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'CODE',
		},
		gpl: {
			libelle: 'nom', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_gpl.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'CODE',
		},
		gnv: {
			libelle: 'nom', commune: 'COMMUNE',
			iconMaxRes: 50, icon: { src: 'assets/map/p_gnv.svg', scale: 1, anchor: [0.5, 1] },
			linesNear: true,
			isEvent: false,
			idField: 'CODE',
		},
		free: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_free.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: false,
			idField: 'id',
		},
		// --------------------- EVENTS ---------------------------------------
		accident: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_accident.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		bouchon: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_bouchon.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		chantier: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_chantier.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		greve: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_greve.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		information: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_information.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		manifestation: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_manifestation.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		meteo: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_meteo.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		obstacle: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_obstacle.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		restriction: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_restriction.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		panne: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_panne.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
		plans_d_urgence: {
			iconMaxRes: 50, icon: { src: 'assets/map/p_plans_d_urgence.png', scale: 1, anchor: [0.5, 1] },
			linesNear: false,
			isEvent: true,
			idField: 'code',
		},
	};

	export const events = ['accident', 'bouchon', 'chantier', 'greve', 'information', 'manifestation', 'meteo', 'obstacle', 'restriction', 'panne', 'plans_d_urgence'];

	export function displayLinesNear(type: string): boolean {
		return types[type] && types[type].linesNear || false;
	}

	export function isEvent(type: string): boolean {
		return types[type] && types[type].isEvent || false;
	}

	export function idField(type: string): string {
		return types[type] && types[type].idField || '';
	}
}
