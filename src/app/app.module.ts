import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WINDOW_PROVIDERS } from './window.provider';
import { HorairesLignesComponent } from '@pages/horaires-lignes/horaires-lignes.component';
import { DetailLigneComponent } from '@pages/detail-ligne/detail-ligne.component';
import { LogoLigneComponent } from '@components/logo-ligne/logo-ligne.component';
import { DetailProchainspassagesComponent } from '@components/detail-prochainspassages/detail-prochainspassages.component';
import { MListWrapperComponent } from '@components/m-list-wrapper/m-list-wrapper.component';
import { IconTypeComponent } from '@components/icon-type/icon-type.component';
import { RealtimeIconComponent } from '@components/realtime-icon/realtime-icon.component';
import { GroupByPipe } from '@pipes/groupBy.pipe';
import { CommunePipe } from '@pipes/commune.pipe';
import { LibellePipe } from '@pipes/libelle.pipe';
import { FilterPipe } from '@pipes/filter.pipe';
import { TimePipe, TimeUnitPipe, TimeSchedulePipe } from '@pipes/time.pipe';
import { ChartComponent } from '@components/chart/chart.component';
import { OccupancyDatasetPipe } from '@features/occupancy/occupancy-dataset.pipe';
import { VerticalBarChartType } from '@components/chart/types/vertical-bar.chart-type';
import { OccupancyChartDialogComponent } from '@features/occupancy/occupancy-chart-dialog/occupancy-chart-dialog.component';
import { ContributionFirstStepComponent } from './pages/contribution/contribution-first-step/contribution-first-step.component';
import { ContributionSecondStepComponent } from './pages/contribution/contribution-second-step/contribution-second-step.component';
import { ContributionFinalStepComponent } from './pages/contribution/contribution-final-step/contribution-final-step.component';
import { ContributionWrapperComponent } from './pages/contribution/contribution-wrapper/contribution-wrapper.component';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { SearchInputComponent } from './features/search/search-input/search-input.component';
import { SearchListComponent } from './features/search/search-list/search-list.component';
import { SearchItemDirective } from './features/search/search-item.directive';
import { IdPipe } from './pipes/id.pipe';
import { ChartTooltipDirective } from './components/chart/chart-tooltip.directive';

@NgModule({
	declarations: [
		AppComponent,
		HorairesLignesComponent,
		LogoLigneComponent,
		GroupByPipe,
		MListWrapperComponent,
		IconTypeComponent,
		CommunePipe,
		LibellePipe,
		DetailLigneComponent,
		FilterPipe,
		DetailProchainspassagesComponent,
		RealtimeIconComponent,
		TimePipe,
		TimeUnitPipe,
		TimeSchedulePipe,
		ChartComponent,
		OccupancyDatasetPipe,
		OccupancyChartDialogComponent,
		ContributionFirstStepComponent,
		ContributionSecondStepComponent,
		ContributionFinalStepComponent,
		ContributionWrapperComponent,
		BackButtonComponent,
		SearchInputComponent,
		SearchListComponent,
		SearchItemDirective,
		IdPipe,
		ChartTooltipDirective
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		MatCardModule,
		MatSidenavModule,
		MatIconModule,
		MatProgressSpinnerModule,
		MatListModule,
		MatDialogModule,
		MatButtonModule,
		MatRippleModule,
		MatExpansionModule,
		MatFormFieldModule,
		MatInputModule,
		MatProgressBarModule,
		MatToolbarModule,
		MatSnackBarModule,
	],
	providers: [
		WINDOW_PROVIDERS,
		// ChartTypes
		// { provide: 'ChartType', useClass: ScatterGraphChartType, multi: true },
		// { provide: 'ChartType', useClass: LineChartType, multi: true },
		{ provide: 'ChartType', useClass: VerticalBarChartType, multi: true },
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
