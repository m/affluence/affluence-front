import { Injectable } from '@angular/core';
import { fromEvent, Observable, Subject } from 'rxjs';

/**
 * The visibilitychange event may not be trigger after a long period in background.
 * A setTimeout system is used in background to ensure the visibility event.
 */

@Injectable({
	providedIn: 'root'
})
export class AppVisibilityService {
	private visibility: Subject<boolean>;
	private backgroundTimeout: any;
	private backgroundTimeoutDuration = 900;
	private _hidden: boolean;

	constructor() {
		this.visibility = new Subject();
		fromEvent(document, 'visibilitychange').subscribe(() => {
			this.visibility.next(!document.hidden);
			if (document.hidden) {
				this.startBackgroundTimeout();
			} else {
				this.clearBackgroundTimeout();
			}
		});
	}

	get visibility$(): Observable<boolean> {
		return this.visibility.asObservable();
	}

	private startBackgroundTimeout() {
		this.clearBackgroundTimeout();
		this.backgroundTimeout = setTimeout(() => {
			if (document.hidden !== this._hidden) {
				this._hidden = document.hidden;
			}
			if (!this._hidden) {
				this.visibility.next(!this._hidden);
				this.clearBackgroundTimeout();
			} else {
				this.startBackgroundTimeout();
			}
		}, this.backgroundTimeoutDuration);
	}

	private clearBackgroundTimeout() {
		if (this.backgroundTimeout) {
			clearTimeout(this.backgroundTimeout);
		}
	}
}
