import { Injectable } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Style } from '@helpers/style.helper';
import { Observable } from 'rxjs';


@Injectable({
	providedIn: 'root'
})
export class BreakpointService {

	breakpoint: Observable<BreakpointState>;

	constructor(private breakpointObserver: BreakpointObserver) {
		this.breakpoint = this.breakpointObserver.observe([`(min-width: ${Style.mpwaBreakpoint})`]);
	}

}
