import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DataService } from '@services/data.service';
import { WINDOW } from '../window.provider';
import { environment } from '../../environments/environment';
import { cleanString } from '@helpers/string.helper';

@Injectable({
	providedIn: 'root'
})
export class ApiMService {
	public domain = 'https://data.mobilites-m.fr';
	public urlApi = `${this.domain}/api`;
	public defaultLineColorConfig = {
		dark: {
			color: 'B49BDA',
			textColor: '000000'
		},
		light: {
			color: '6A35B4',
			textColor: '26222A'
		}
	};

	lignes: any;
	private _lignesObj: BehaviorSubject<any> = new BehaviorSubject([]);
	public readonly lignesObj: Observable<any> = this._lignesObj.asObservable();
	private _lignesTypeObj: BehaviorSubject<any> = new BehaviorSubject([]);
	public readonly lignesTypeObj: Observable<any> = this._lignesTypeObj.asObservable();

	lineTypes: any;
	lineObjs: any;

	clustersObj: any;
	objLignes: any;
	constructor(
		public http: HttpClient,
		public dataservice: DataService,
		@Inject(WINDOW) private window: Window
	) {
		this.domain = this.getDomain();
		this.urlApi = `${this.domain}/api`;
	}

	private getDomain() {
		if (environment.api === 'local') return 'http://localhost:3000';
		else if (environment.api === 'test') return 'https://datatest.mobilites-m.fr';
		else if (environment.api === 'preprod') return 'https://data-pp.mobilites-m.fr';
		else if (environment.api === 'testOuProd') {
			if (['apptest.mobilites-m.fr', 'pwa.passmobilites.app'].includes(window.location.hostname)) return 'https://data-pp.mobilites-m.fr';
			else return 'https://data.mobilites-m.fr';
		}
		return this.domain;
	}

	get(path: string, options?: any): Observable<any> {
		return this.http.get(this.urlApi + path, options)
			.pipe(
				catchError(this.handleError)
			);
	}

	async getItineraires(requests: any[]) {
		const requestPromises: any[] = [];

		for (const request of requests) {
			const PReq = this.http.get(request.url).toPromise();
			requestPromises.push(PReq);
		}
		const PItineraire = Promise.all(requestPromises);
		const itineraire = await PItineraire;
		return itineraire;
	}

	async getPolyTroncons() {
		const searchUrl = '/troncons/poly?';
		const polyTroncon = await this.http.get(this.urlApi + searchUrl).toPromise();
		return polyTroncon;
	}
	getLignes(): Promise<boolean> {
		return new Promise((resolve) => {
			this.get('/routers/default/index/routes').subscribe((data: any) => {
				this.lignes = data;
				const obj = {};
				const objType = {};
				for (const l of data) {
					obj[l.id] = l;
					if (!objType[l.type]) objType[l.type] = [];
					objType[l.type].push(l);
					if (l.color === '' && l.textColor === '') {
						l.color = this.defaultLineColorConfig.dark.color;
						l.textColor = this.defaultLineColorConfig.dark.textColor;
					}
				}
				this.lineObjs = obj;
				this.lineTypes = objType;
				resolve(true);
			});
		});
	}
	getLigne(ligneCode: string) {
		return this.lineObjs[ligneCode];
	}
	async takeLigne(idLigne) {
		if (!this.objLignes) {
			const data = await this.takeLignes();
			this.objLignes = data;
		}
		return this.findLigne(idLigne)
	}
	async takeLignes() {
		const lignes = await this.get('/routers/default/index/routes').toPromise();
		return lignes;
	}
	findLigne(idLigne) {
		const index = this.objLignes.findIndex((lign) => lign.id === idLigne);
		const ligne = this.objLignes[index];
		return ligne;
	}

	async getPoi(id: string, type: string) {
		return this.get(`/points/json?types=${type}&codes=${id}`).toPromise();
	}

	getCitizPicture(code: string) {
		return this.urlApi + '/citiz/' + code + '/photo';
	}

	private handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			console.error('An error occurred:', error.error.message);
		} else {
			console.error(`Backend returned code ${error.status}, body was:`, error.error);
		}
		return throwError('Une erreur est survenue; reessayez plus tard.');
	}

	parseResultPP(res, nbEl?) {
		if (!res) return;
		const prochainesPassages = {};
		res.forEach(dest => {
			const ligneCode = dest.pattern.id.split(':')[0] + ':' + dest.pattern.id.split(':')[1];
			const ligne = this.lineObjs[ligneCode];
			const desc = dest.pattern.desc;
			if (!prochainesPassages[desc]) {
				const city = dest.pattern.lastStopName && dest.pattern.lastStopName.includes(',') ? dest.pattern.lastStopName.split(',')[0] : undefined;
				prochainesPassages[desc] = {
					pattern: dest.pattern,
					name: desc,
					ligne: ligne,
					time: [],
					id: cleanString(`${ligne.id}:${dest.pattern.desc}`),
					city: city
				};
			}
			if (nbEl) {
				for (let i = 0; i < dest.times.length && i < nbEl; i++) {
					prochainesPassages[desc].time.push(dest.times[i]);
				}
			} else {
				for (let i = 0; i < dest.times.length; i++) {
					prochainesPassages[desc].time.push(dest.times[i]);
				}
			}
		});
		const prochPassag = [];
		for (const el in prochainesPassages) {
			if (prochainesPassages[el].time.length > 0) {
				prochPassag.push(prochainesPassages[el]);
			}
		}
		prochPassag.forEach(element => {
			element.time.sort(function (a, b) {
				const depA = (a.realtimeDeparture ? a.realtimeDeparture : a.sheduledDeparture) + a.serviceDay;
				const depB = (b.realtimeDeparture ? b.realtimeDeparture : b.sheduledDeparture) + b.serviceDay;

				if (depA > depB) {
					return 1;
				}
				if (depA < depB) {
					return -1;
				}
				return 0;
			});
		});
		prochPassag.sort(function (a, b) {
			if (!a || a.time.length == 0) return -1;
			if (!b.time || b.time.length == 0) return 1;
			const depA = (a.time[0].realtimeDeparture ? a.time[0].realtimeDeparture : a.time[0].sheduledDeparture) + a.time[0].serviceDay;
			const depB = (b.time[0].realtimeDeparture ? b.time[0].realtimeDeparture : b.time[0].sheduledDeparture) + b.time[0].serviceDay;
			if (depA > depB) {
				return 1;
			}
			if (depA < depB) {
				return -1;
			}
			return 0;
		});
		return prochPassag.filter((pp: any) => !pp.pattern.desc.includes('SANS VOYAGEUR'));
	}
}
