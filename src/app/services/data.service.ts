import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DataTypes } from '@helpers/types.helper';


@Injectable({
	providedIn: 'root'
})
export class DataService {
	citizYea: any;
	private _recherchesRecentes: any[] = [];
	longeurRecherchesRecentes = 3;
	private _form: any = null;
	geom: BehaviorSubject<any> = new BehaviorSubject([]);
	mobileScreen = false;
	private _itineraireData: { params: any, itineraires: any[] };
	height: number;
	top: number;
	bottomSheetTop: number = window.innerHeight * 0.45;
	sizes: BehaviorSubject<any> = new BehaviorSubject({});

	public get form(): any {
		return this._form;
	}
	public set form(value: any) {
		this._form = value;
	}

	public get itineraireData(): { params: any, itineraires: any[] } {
		return this._itineraireData;
	}
	public set itineraireData(value: { params: any, itineraires: any[] }) {
		this._itineraireData = value;
	}

	private _pointsAffiches: BehaviorSubject<any> = new BehaviorSubject([]);
	public readonly pointsAffiches: Observable<any> = this._pointsAffiches.asObservable();
	lignes = {};

	types = DataTypes.types;

	event_types = DataTypes.events;

	constructor() {
		// this.load();
		if (window.innerWidth <= 768) this.mobileScreen = true;
	}

	public compareItineraireParams(params1: any, params2: any): boolean {
		let same = true;
		for (const key in params1) {
			if (params1[key] !== params2[key]) {
				same = false;
			}
		}
		return same;
	}

	updatePointsAffiches(points) {
		this._pointsAffiches.next(points);
	}

	getLibellePoint(point) {
		let champ = this.types[point.properties.type].libelle;
		return point.properties[champ];
	}

	getCommunePoint(point) {
		let champ = this.types[point.properties.type].commune;
		return point.properties[champ];
	}



	getDurationTime(duration: any) {
		let hour, minute, seconds;
		seconds = duration;
		if (duration < 60) return seconds + ' s';
		minute = Math.floor(seconds / 60);
		hour = Math.floor(minute / 60);
		minute = minute % 60;
		hour = hour % 24;
		//Préparer les bonnes chaines (1h20min, 30min)
		if (hour == 0) return minute + ' min'
		return hour + ' h ' + minute + ' min'
	}
}
