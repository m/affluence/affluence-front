import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

export const assetsPath = 'assets/map/svg';

export const iconsMapping = {
	covoiturage_2: `${assetsPath}/covoiturage_2+.svg`,
	accident: `${assetsPath}/accident.svg`,
	bouchon: `${assetsPath}/bouchon.svg`,
	chantier: `${assetsPath}/chantier.svg`,
	greve: `${assetsPath}/greve.svg`,
	information: `${assetsPath}/information.svg`,
	manifestation: `${assetsPath}/manifestation.svg`,
	meteo: `${assetsPath}/meteo.svg`,
	obstacle: `${assetsPath}/obstacle.svg`,
	restriction: `${assetsPath}/restriction.svg`,
	panne: `${assetsPath}/panne.svg`,
	agenceM: `${assetsPath}/agenceM.svg`,
	autostop: `${assetsPath}/autostop.svg`,
	citiz: `${assetsPath}/citiz.svg`,
	citizyea: `${assetsPath}/citizyea.svg`,
	clusters: `${assetsPath}/clusters.svg`,
	gnv: `${assetsPath}/gnv.svg`,
	gpl: `${assetsPath}/gpl.svg`,
	hydrogene: `${assetsPath}/hydrogene.svg`,
	MVA: `${assetsPath}/MV_agence.svg`,
	MVC: `${assetsPath}/MV_Consigne.svg`,
	parkingCov: `${assetsPath}/parkingCov.svg`,
	pointCov: `${assetsPath}/pointCov.svg`,
	pointService: `${assetsPath}/pointService.svg`,
	recharge: `${assetsPath}/recharge.svg`,
	depositaire: `${assetsPath}/relaisTAG.svg`,
	PKG: `${assetsPath}/PKG_0.svg`,
	PAR: `${assetsPath}/PR_0.svg`,
};

@Component({
	selector: 'app-icon-type',
	templateUrl: './icon-type.component.html',
	styleUrls: ['./icon-type.component.scss']
})
export class IconTypeComponent {

	@Input() type: string;
	@Input() isMatList: boolean;

	mapping = iconsMapping;

	constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
		for (const type in this.mapping) {
			if (this.mapping.hasOwnProperty(type)) {
				const iconPath = this.mapping[type];
				this.matIconRegistry.addSvgIcon(type, this.domSanitizer.bypassSecurityTrustResourceUrl(iconPath));
			}
		}
	}

}
