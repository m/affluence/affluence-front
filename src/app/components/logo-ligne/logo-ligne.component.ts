import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
	selector: 'app-logo-ligne',
	template: `
	<svg [ngStyle]="{'width.px':width, 'height.px':height, 'background':'#'+ligne?.color}" [attr.viewBox]="viewBoxSize">
  		<title>{{ligne?.longName}}</title>
    	<!--<ng-container *ngIf="this.cercle; else rectangle">
     		<circle class="svgFond" cx="50" cy="50" r="50" stroke="white" stroke-width="0" [attr.fill]="'#'+ligne?.color" />
    	</ng-container>
    	<ng-template #rectangle>
    		<rect class="svgFond" [attr.x]="x" y="10" [attr.width]="widthRect" stroke="white" stroke-width="0" height="80" rx="5" ry="5"  [attr.fill]="'#'+ligne?.color" />
    	</ng-template>-->
    	<text class="svgNumLigne" x="50%" y="50%" text-anchor="middle" dy="15px" [attr.fill]="'#'+ligne?.textColor">{{ligne?.shortName}}</text>
  	</svg>
  `,
	styles: [
		':host, svg { display: block; } svg { transition: all 250ms; }',
		'svg { font-size: 40px; font-family: Roboto; font-weight:bold; stroke-width:6px; }',
	]
})
export class LogoLigneComponent implements OnInit {
	@Input() ligne: any;
	@Input() @HostBinding('style.min-height.px') height: number;
	@Input() @HostBinding('style.min-width.px') width: number;
	// width:number;
	viewBoxSize: any;
	id: any;
	x: string;
	widthRect: string;
	cercle: boolean;

	constructor() { }

	ngOnInit() {
		if (!this.ligne) return 'Erreur ligne';
		if (this.ligne.type === 'SNC') {
			this.ligne.shortName = 'TER';
		}
		if (this.ligne.type === 'TRAM' || this.ligne.type === 'CHRONO') {// Cercle
			this.cercle = true;
			this.viewBoxSize = '0 0 100 100';
			if (!this.width) this.width = this.height;
		} else if (this.ligne.type === 'FLEXO' || this.ligne.type === 'PROXIMO' || this.ligne.type === 'NAVETTE' || this.ligne.shortName.length <= 2) {// carré
			this.cercle = false;
			this.viewBoxSize = '0 0 100 100';
			// this.x = '5';
			// this.widthRect = '90';
			if (!this.width) this.width = this.height;
		} else {// rectangle
			this.cercle = false;
			this.viewBoxSize = '0 0 125 100';
			// this.x = '10';
			// this.widthRect = '172';
			if (!this.width) this.width = 88;

		}
	}
}
