import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { BreakpointService } from '@services/breakpoint.service';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-back-button',
	templateUrl: './back-button.component.html',
	styleUrls: ['./back-button.component.scss'],
})
export class BackButtonComponent implements OnInit, OnDestroy {

	isMobile: boolean;

	private unsubscriber = new Subject<void>();

	constructor(private breakpointService: BreakpointService) { }

	ngOnInit(): void {
		this.breakpointService.breakpoint.pipe(takeUntil(this.unsubscriber)).subscribe((result) => {
			this.isMobile = !result.matches;
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	back() {
		history.back();
	}

}
