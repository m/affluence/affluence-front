import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppVisibilityService } from '@services/app-visibility.service';
import { groupByField } from '@helpers/array.helper';
import { RealtimeDataService } from '@features/realtime-data/realtime-data.service';
import { NextStop } from '@features/realtime-data/realtime-data.model';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { OccupancyChartDialogComponent } from '@features/occupancy/occupancy-chart-dialog/occupancy-chart-dialog.component';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-detail-prochainspassages',
	templateUrl: './detail-prochainspassages.component.html',
	styleUrls: ['./detail-prochainspassages.component.scss']
})
export class DetailProchainspassagesComponent implements OnInit, OnDestroy {
	@Input() lines: any[];
	@Input() avecAccordeon: any;
	@Input() shouldBeUpdated = true;
	@Input() showLogo = true;
	@Input() mode: 'default' | 'list-item' | 'compact' | 'compact-unique' | 'with-occupancy' = 'default';
	@Output() nbreLignes: EventEmitter<number> = new EventEmitter<number>();
	@Input() logoHeight: number;
	@Input() displayTimes = true;
	prochPassages: NextStop[] = [];
	prochPassagesCompact = [];
	displayedColumns: string[] = ['logo', 'desc', 'timeone', 'timetwo'];
	expandedElement: any;
	url;
	unavailable = false;
	enableContrib = environment.enableContrib;

	private paused = false;
	private _zoneArret: any;
	intervalId;
	unsubscriber = new Subject<void>();


	@Input() set zoneArret(obj: any) {
		this._zoneArret = obj;
		this.prochPassages = [];
		this.prochPassagesCompact = [];
		this.chargeData();
		if (this.intervalId) {
			clearInterval(this.intervalId);
		}
		this.intervalId = setInterval(() => {
			this.chargeData();
		}, 20000);
	}
	get zoneArret() {
		return this._zoneArret;
	}


	constructor(
		private realtimeDataService: RealtimeDataService,
		private appVisibilityService: AppVisibilityService,
		private dialog: MatDialog
	) { }

	ngOnInit() {
		this.appVisibilityService.visibility$.pipe(takeUntil(this.unsubscriber)).subscribe((visible) => {
			if (!visible && this.intervalId) {
				clearInterval(this.intervalId);
			} else {
				this.chargeData();
				this.intervalId = setInterval(() => {
					this.chargeData();
				}, 20000);
			}
		});
	}

	chargeData() {
		this.unavailable = false;
		const lineIds = this.lines.map((line: any) => line.id) || [];
		this.realtimeDataService.getStoptimes(this.zoneArret.type, this.zoneArret.code, lineIds).subscribe(res => {
			const nbEltToDisplay = 2;
			if (this.shouldBeUpdated) {
				const passages: NextStop[] = groupByField(this.realtimeDataService.parseResultPP(res, nbEltToDisplay), 'ligne.id');
				// Add/Update objects instead of replace the entire list.
				passages.forEach((passage: any) => {
					const p: NextStop = this.prochPassages.find((pp) => passage.id === pp.id);
					if (!p) {
						passage.opened = false;
						this.prochPassages.push(passage);
					} else {
						p.time = passage.time;
					}
				});

				// Remove old objects from the list.
				this.prochPassages.forEach((pp, index) => {
					if (!passages.find((p) => p.id === pp.id)) {
						this.prochPassages.splice(index, 1);
					}
				});
				if (this.prochPassages.length === 0) {
					this.unavailable = true;
				}

				// Set the stopId property from the time values.
				this.prochPassages.forEach(passage => {
					passage.stopId = passage.time[0]?.stopId;
				});

				if (this.mode === 'compact-unique' && this.lines) {
					this.prochPassages = this.prochPassages.filter(proch => {
						return proch.ligne.id === this.lines[0].id;
					});
					this.nbreLignes.emit(this.unavailable ? -1 : this.prochPassages.length);
				}
				if (this.mode === 'compact' && this.prochPassages.length > 0) {
					this.prochPassagesCompact = [];
					this.prochPassages.forEach(el => {
						if (!this.prochPassagesCompact[el.ligne.id]) {
							this.prochPassagesCompact[el.ligne.id] = {
								object: [el],
							};
						} else {
							this.prochPassagesCompact[el.ligne.id].object.push(el);
						}
					});
				}
			}
		}, (err) => this.unavailable = true);
	}
	ngOnDestroy() {
		if (this.intervalId) {
			clearInterval(this.intervalId);
		}
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	openChartDialog(obj: NextStop) {
		this.dialog.open(OccupancyChartDialogComponent, {
			data: obj,
			panelClass: 'occupancy-chart-dialog',
		});
	}

}
