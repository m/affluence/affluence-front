import * as d3 from 'd3';

export enum ChartPadding {
	TOP = 0,
	RIGHT = 1,
	BOTTOM = 2,
	LEFT = 3,
}

export interface Point {
	x: number | string;
	y: number | string;
}

export interface Axis {
	grouped?: boolean;
	isDate?: boolean;
	tooltipFormatter?: (value: any, point?: Point, dataset?: Dataset) => string;
}

export interface Dataset {
	points: Point[][];
	axis?: {
		x?: Axis;
		y?: Axis;
	};
	options?: {
		padding?: number | [number, number, number, number],
		empty?: boolean,
		unavailable?: boolean,
		ghost?: boolean,
	}
}

export interface ChartContext {
	height: number;
	width: number;
	chart: d3.Selection;
}

export interface ChartType {
	readonly type: string;

	padding: number;
	scaleX: d3.Scale;
	scaleY: d3.Scale;

	udpate(dataset: Dataset, context: ChartContext): void;

	destroy(context: ChartContext): void;

	globalMouseUpdate(dataset: Dataset, context: ChartContext, event: MouseEvent): void;
}
