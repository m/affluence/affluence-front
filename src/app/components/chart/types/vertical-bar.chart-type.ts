import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { ChartType, Dataset, ChartContext, Point, ChartPadding } from '../chart.model';
import { BaseChartType } from './BaseChartType';

@Injectable()
export class VerticalBarChartType extends BaseChartType implements ChartType {

	protected radius = 2;
	protected staggerTime = 100;

	get type(): string {
		return 'vertical-bar';
	}

	getXScale(dataset: Dataset, context: ChartContext): d3.Scale {
		return d3.scaleBand()
			.domain(this.getXDomain(dataset))
			.range([8, context.width - this.getPadding(ChartPadding.RIGHT, dataset) * 2]);
	}

	getYScale(dataset: Dataset, context: ChartContext): d3.Scale {
		return d3.scaleLinear()
			.domain([0, d3.max(this.getYDomain(dataset))])
			.range([context.height - (this.getPadding(ChartPadding.BOTTOM, dataset) * 2), this.getPadding(ChartPadding.TOP, dataset) * 2]);
	}

	getYAxis(dataset: Dataset, context: ChartContext) {
		const axis = super.getYAxis(dataset, context);
		axis.ticks(4);
		return axis;
	}

	udpate(dataset: Dataset, context: ChartContext): void {
		const grouped = dataset.axis && dataset.axis.x && dataset.axis.x.grouped;
		const scaleX = this.getXScale(dataset, context);
		const scaleY = this.getYScale(dataset, context);
		const barWidth = grouped ? scaleX.bandwidth() / dataset.points.length : scaleX.bandwidth();

		// Create axis:
		this.buildAxis(dataset, context, this.getXAxis(dataset, scaleX), this.getYAxis(dataset, scaleY));

		const x = (p: Point, index: number) => {
			let _x = scaleX(p.x) + barWidth / 2;
			if (grouped) {
				_x += index * (this.getPadding(ChartPadding.LEFT) * 2);
			}
			return _x;
		};

		// Create bars:
		dataset.points.forEach((points: Point[], i: number) => {
			const barsSelection = context.chart.selectAll(`.${this.type}-${i}`).data(points);
			barsSelection
				.transition(this.transitionDuration * 4)
				.attr('d', (p: Point) => {
					const height = context.height - scaleY(p.y) - this.getPadding(ChartPadding.BOTTOM, dataset) * 2;
					return this.topRoundedColumn(x(p, i), scaleY(p.y), height, barWidth);
				})
				.attr('class', p => {
					let classes = `${this.type} ${this.type}-${i} clickable`;
					if (dataset.options.ghost) {
						classes += ' ghost';
					}
					return classes;
				})
				.style('opacity', dataset.options.ghost ? 0.3 : 0.9)
				.attr('data-point', p => JSON.stringify(p));
			barsSelection.enter()
				.append('path')
				.attr('class', p => {
					let classes = `${this.type} ${this.type}-${i} clickable`;
					if (dataset.options.ghost) {
						classes += ' ghost';
					}
					return classes;
				})
				.style('cursor', 'pointer')
				.attr('data-point', p => JSON.stringify(p))
				.attr('d', (p: Point) => {
					return this.topRoundedColumn(x(p, i), context.height - this.getPadding(ChartPadding.BOTTOM, dataset) * 2, 0, barWidth);
				})
				.style('opacity', 0)
				.transition(this.transitionDuration * 4)
				// .delay((point: Point, index: number) => index * this.staggerTime)
				.style('opacity', dataset.options.ghost ? 0.3 : 0.9)
				.attr('d', (p: Point) => {
					const height = context.height - scaleY(p.y) - this.getPadding(ChartPadding.BOTTOM, dataset) * 2;
					return this.topRoundedColumn(x(p, i), scaleY(p.y), height, barWidth);
				});
			barsSelection.exit()
				.transition(this.transitionDuration * 4)
				.style('opacity', 0)
				.attr('d', (p: Point) => {
					return this.topRoundedColumn(x(p, i) || 0, context.height - this.getPadding(ChartPadding.BOTTOM, dataset) * 2, 0, barWidth);
				})
				.remove();
		});
	}

	destroy(context: ChartContext): void {
		context.chart.selectAll(`.${this.type}`)
			.transition(this.transitionDuration)
			.attr('r', 0)
			.remove();
	}

	private topRoundedColumn(x: number, y: number, height: number, width: number) {
		const path = d3.path();
		path.moveTo(x, y + height);
		path.lineTo(x, y + this.radius);
		path.quadraticCurveTo(x, y, x + this.radius, y);
		path.lineTo(x + width - this.radius, y);
		path.quadraticCurveTo(x + width, y, x + width, y + this.radius);
		path.lineTo(x + width, y + height);
		path.closePath();
		return path;
	}
}
