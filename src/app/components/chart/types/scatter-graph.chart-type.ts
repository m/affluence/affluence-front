import { Injectable } from '@angular/core';
import { ChartType, Dataset, ChartContext, Point } from '../chart.model';
import { BaseChartType } from './BaseChartType';

@Injectable()
export class ScatterGraphChartType extends BaseChartType implements ChartType {

	protected radius = 3;
	protected staggerTime = 25;

	get type() {
		return 'scatter-graph';
	}

	udpate(dataset: Dataset, context: ChartContext): void {
		super.update(dataset, context);

		// Create user lines.
		this.buildUserLines(context);

		// Create scatter-graph points.
		const className = `${this.type}-point`;
		dataset.points.forEach((points: Point[], i: number) => {
			const selection = context.chart.selectAll(`.${className}-${i}`).data(points);
			// Update elements.
			selection
				.transition(this.transitionDuration)
				.attr('cx', (p: Point) => this.scaleX(p.x))
				.attr('cy', (p: Point) => this.scaleY(p.y));
			// Create new elements.
			selection.enter()
				.append('circle')
				.attr('class', `${className} ${className}-${i}`)
				.attr('cx', (p: Point) => this.scaleX(p.x))
				.attr('cy', (p: Point) => this.scaleY(p.y))
				.attr('r', 0)
				.style('opacity', 0.5)
				.transition()
				.duration(this.transitionDuration)
				// .delay((point: Point, _i: number) => _i * this.staggerTime)
				.attr('r', this.radius);
			// Remove elements.
			selection.exit()
				.transition(this.transitionDuration)
				.attr('r', 0)
				.remove();
		});
	}

	destroy(context: ChartContext) {
		context.chart.selectAll(`.${this.type}-point`)
			.transition(this.transitionDuration)
			.attr('r', 0)
			.remove();
	}

}
