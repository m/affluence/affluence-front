import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { ChartType, Dataset, ChartContext, Point } from '../chart.model';
import { BaseChartType } from './BaseChartType';

@Injectable()
export class LineChartType extends BaseChartType implements ChartType {

	get type(): string {
		return 'line';
	}

	udpate(dataset: Dataset, context: ChartContext): void {
		super.update(dataset, context);

		// Create user lines.
		this.buildUserLines(context);

		const line = d3.line()
			.x((p: Point) => this.scaleX(p.x))
			.y((p: Point) => this.scaleY(p.y));

		const selection = context.chart.selectAll(`.${this.type}`);
		selection.datum((points: Point[], i: number) => dataset.points[i])
			.transition()
			.duration(this.transitionDuration * 4)
			.attr('d', line);

		selection.data(dataset.points)
			.enter()
			.append('path')
			.datum((points: Point[], i: number) => {
				return points.map(p => {
					return { ...p, y: 0 };
				});
			})
			.attr('class', (points: Point[], i: number) => {
				return `${this.type} ${this.type}-${i}`;
			})
			.attr('fill', 'none')
			.attr('d', line)
			.datum((points: Point[], i: number) => dataset.points[i])
			.transition()
			.duration(this.transitionDuration * 4)
			.attr('d', line);
	}

	destroy(context: ChartContext): void {
		context.chart.selectAll(`.${this.type}`)
			.remove();
	}
}
