import * as d3 from 'd3';
import { Dataset, ChartContext, Point, ChartPadding } from '../chart.model';

export abstract class BaseChartType {

	padding = 16;
	protected transitionDuration = 250;
	protected axisLineStaggerTime = 25;
	protected displayUserLines = false;
	protected displayAxis = false;

	scaleX: d3.Scale;
	scaleY: d3.Scale;

	getPadding(padding: number | ChartPadding = ChartPadding.TOP, dataset?: Dataset) {
		if (dataset?.options?.padding) {
			return Array.isArray(dataset.options.padding) ? dataset.options.padding[padding] : dataset.options.padding;
		}
		return Array.isArray(this.padding) ? this.padding[padding] : this.padding;
	}

	update(dataset: Dataset, context: ChartContext) {
		this.scaleX = this.getXScale(dataset, context);
		const xAxis = this.getXAxis(dataset, this.scaleX);

		this.scaleY = this.getYScale(dataset, context);
		const yAxis = this.getYAxis(dataset, this.scaleY);

		// Create axis:
		this.buildAxis(dataset, context, xAxis, yAxis);
	}

	xIsDate(dataset: Dataset): boolean {
		return dataset.axis && dataset.axis.x && dataset.axis.x.isDate;
	}

	yIsDate(dataset: Dataset): boolean {
		return dataset.axis && dataset.axis.y && dataset.axis.y.isDate;
	}

	getXDomain(dataset: Dataset): any[] {
		const domain = [].concat(...dataset.points).map((p: Point) => p.x);
		if (dataset.axis && dataset.axis.x && dataset.axis.x.grouped) {
			return [...new Set(domain)];
		}
		return domain;
	}

	getXScale(dataset: Dataset, context: ChartContext): d3.Scale {
		let scaleX: d3.Scale;
/* 		if (this.xIsDate(dataset)) {
			const _dates = this.getXDomain(dataset);
			scaleX = d3.scaleTime()
				.domain([d3.min(_dates), d3.max(_dates)]);
		} else { */
			scaleX = d3.scaleLinear()
				.domain([0, d3.max(this.getXDomain(dataset))]);
				//.domain([0, 68]);

		/* } */
		scaleX.range([this.getPadding(ChartPadding.LEFT, dataset) * 2, context.width - this.getPadding(ChartPadding.RIGHT, dataset) * 2]);
		return scaleX;
	}

	getXAxis(dataset: Dataset, scaleX: d3.Scale) {
		const xAxis = d3.axisBottom(scaleX)    
		.tickValues(scaleX.domain().filter((d, i) => d % 3600 === 0 && d % 7200 !== 0))
		.tickFormat((d) => `${d / 3600}h`)
		//.tickValues(scaleX.domain().filter((d, i) => d % 100 === 0));

		//xAxis.tickValues([0,10,20,30,40,50,60]);
/* 		if (this.xIsDate(dataset)) {
			xAxis.tickFormat(d3.timeFormat('%d/%m/%Y'));
		} */
		return xAxis;
	}

	getYDomain(dataset: Dataset): any[] {
		const domain = [].concat(...dataset.points).map((p: Point) => p.y);
		return [...new Set(domain)];
	}

	getYScale(dataset: Dataset, context: ChartContext): d3.Scale {
		let scaleY: d3.Scale;
		if (this.yIsDate(dataset)) {
			const _dates = this.getYDomain(dataset);
			scaleY = d3.scaleTime()
				.domain([d3.min(_dates), d3.max(_dates)]);
		} else {
			scaleY = d3.scaleLinear()
				.domain([0, d3.max(this.getYDomain(dataset))]);
		}
		scaleY.range([context.height - (this.getPadding(ChartPadding.BOTTOM, dataset) * 2), this.getPadding(ChartPadding.TOP, dataset) * 2]);
		return scaleY;
	}

	getYAxis(dataset: Dataset, scaleY: d3.Scale) {
		const yAxis = d3.axisLeft(scaleY);
		if (this.yIsDate(dataset)) {
			yAxis.tickFormat(d3.timeFormat('%d/%m/%Y'));
		}
		return yAxis;
	}

	buildAxis(dataset: Dataset, context: ChartContext, axisX: any, axisY: any) {
		const paddingRight = this.getPadding(ChartPadding.RIGHT, dataset);
		const paddingBottom = this.getPadding(ChartPadding.BOTTOM, dataset);
		const paddingLeft = this.getPadding(ChartPadding.LEFT, dataset);
		const xAxisSelection = context.chart.selectAll('.x-axis').data(['x']);
		xAxisSelection
			.transition(this.transitionDuration)
			.call(axisX);
		xAxisSelection
			.enter()
			.append('g')
			.attr('class', 'axis x-axis')
			.attr('transform', `translate(0, ${context.height - (paddingBottom * 2)})`)
			.call(axisX);

		const yAxisSelection = context.chart.selectAll('.y-axis').data(['y']);
		yAxisSelection
			.enter()
			.append('g')
			.attr('class', 'axis y-axis')
			.attr('transform', `translate(${paddingLeft * 2}, 0)`)
			.call(axisY)
			.selectAll('.tick')
			.append('line')
			.attr('class', 'full-width-line')
			.attr('x1', -6)
			.attr('x2', context.width - (paddingRight * 3));
		const select = yAxisSelection
			.call(axisY);
		const ticks = select.selectAll('.tick');
		const customLines = ticks.selectAll('.full-width-line').data(['y'])
		customLines
			.enter()
			.append('line')
			.attr('class', 'full-width-line')
			.attr('x1', -6)
			.attr('x2', context.width - (paddingRight * 3));
		this.displayAxis = true;
	}

	buildUserLines(context: ChartContext) {
		const userLines = context.chart.selectAll('.user-line');
		userLines.selectAll('line')
			.attr('x1', (d: string) => d === 'y' ? 8 : (context.width / 2) + 8)
			.attr('y1', (d: string) => d === 'y' ? 8 : 8)
			.attr('x2', (d: string) => d === 'y' ? context.width - 8 : (context.width / 2) + 8)
			.attr('y2', (d: string) => d === 'y' ? 8 : (context.height) - (8));
		userLines.data(['x', 'y'])
			.enter()
			.append('g')
			.attr('class', (d: string) => `user-line user-line-${d}`)
			.append('line')
			.style('opacity', 0)
			.attr('x1', (d: string) => d === 'y' ? 8 : (context.width / 2) + 8)
			.attr('y1', (d: string) => d === 'y' ? 8 : 8)
			.attr('x2', (d: string) => d === 'y' ? context.width - 8 : (context.width / 2) + 8)
			.attr('y2', (d: string) => d === 'y' ? 8 : (context.height) - (8));
		this.displayUserLines = true;
	}

	updateUserLines(dataset: Dataset, context: ChartContext, x: number, y: number) {
		context.chart.select('svg').style('cursor', 'crosshair');
		context.chart.select('.user-line-y line').style('opacity', 1).attr('y1', y).attr('y2', y);
		context.chart.select('.user-line-x line').style('opacity', 1).attr('x1', x).attr('x2', x);
	}

	globalMouseUpdate(dataset: Dataset, context: ChartContext, event: MouseEvent) {
		if (this.displayUserLines) {
			this.updateUserLines(dataset, context, event.offsetX, event.offsetY);
		}
	}
}
