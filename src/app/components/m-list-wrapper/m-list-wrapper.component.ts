import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
	selector: 'app-m-list-wrapper',
	templateUrl: './m-list-wrapper.component.html',
	styleUrls: ['./m-list-wrapper.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	exportAs: 'mListWrapper'
})
export class MListWrapperComponent implements OnInit {

	@Input() seeMoreEnable = false;

	seeMore = false;

	constructor() { }

	ngOnInit() {
	}

}
