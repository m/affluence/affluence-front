import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { environment } from 'src/environments/environment';
import { takeUntil, filter, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { trigger, transition, style, animate, keyframes, query } from '@angular/animations';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: [
		trigger('fade', [
			transition(':enter', [
				query('.logo-container', [
					animate(500, keyframes([
						style({ transform: 'scale3d(1, 1, 1)', opacity: 0, offset: 0 }),
						style({ transform: 'scale3d(1.25, 0.75, 1)', opacity: 1, offset: 0.3 }),
						style({ transform: 'scale3d(0.75, 1.25, 1)', offset: 0.4 }),
						style({ transform: 'scale3d(1.15, 0.85, 1)', offset: 0.5 }),
						style({ transform: 'scale3d(.95, 1.05, 1)', offset: 0.65 }),
						style({ transform: 'scale3d(1.05, .95, 1)', offset: 0.75 }),
						style({ transform: 'scale3d(1, 1, 1)', offset: 1 }),
					]))
				])
			]),
			transition(':leave', [
				style({ opacity: 1 }),
				animate(500, style({ opacity: 0 }))
			])
		])
	]
})
export class AppComponent implements OnInit, OnDestroy {

	displayLoader = false;

	private unsubscriber = new Subject<void>();

	constructor(
		private router: Router
	) { }

	ngOnInit(): void {
		this.router.events.pipe(
			tap((event) => {
				if (event instanceof NavigationStart) {
					this.displayLoader = true;
				}
			}),
			filter(event => event instanceof NavigationEnd),
			takeUntil(this.unsubscriber)
		).subscribe(event => {
			this.displayLoader = false;
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}
}
