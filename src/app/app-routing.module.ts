import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HorairesLignesComponent } from '@pages/horaires-lignes/horaires-lignes.component';
import { DetailLigneComponent } from '@pages/detail-ligne/detail-ligne.component';
import { LinesResolver } from '@features/lines/lines.resolver';
import { LineResolver } from '@features/line/line.resolver';
import { ContributionFirstStepComponent } from '@pages/contribution/contribution-first-step/contribution-first-step.component';
import { ContributionSecondStepComponent } from '@pages/contribution/contribution-second-step/contribution-second-step.component';
import { ContributionFinalStepComponent } from '@pages/contribution/contribution-final-step/contribution-final-step.component';
import { ContributionWrapperComponent } from '@pages/contribution/contribution-wrapper/contribution-wrapper.component';
import { PatternsResolver } from '@features/patterns/patterns.resolver';

const routes: Routes = [
	{
		path: '', component: HorairesLignesComponent,
		resolve: {
			lines: LinesResolver
		},
		data: {
			title: `Fréquentation aux arrêts | M - Mesure de l'affluence`
		},
	},
	{
		path: 'ligne/:id', component: DetailLigneComponent,
		resolve: {
			clusters: LineResolver
		},
		data: {
			title: `Fréquentation aux arrêts de la ligne @line | M - Mesure de l'affluence`
		},
	},
	{
		path: 'contribution', component: ContributionWrapperComponent,
		data: {
			totalStep: 3,
			title: `Votre contribution (@step/3) | M - Mesure de l'affluence`
		},
		children: [
			{ path: '', component: ContributionFirstStepComponent, data: { step: 1 } },
			{ path: ':cluster', component: ContributionSecondStepComponent, data: { step: 2 }, resolve: { patterns: PatternsResolver } },
			{ path: ':stop/:line/:dir/:headsign', component: ContributionFinalStepComponent, data: { step: 3 } },
		]
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
